using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InputFieldBehavior : MonoBehaviour
{
    private TextMeshProUGUI text;
    private string _text;

    public string Text
    {
        get
        {
            return _text;
        }

        set
        {
            _text = value;
            if(text != null)
            {
                text.text = _text;
            }
        }
    }



    // Start is called before the first frame update
    void Start()
    {
        text = transform.Find("Text Area/Text").gameObject.GetComponent<TextMeshProUGUI>();
        text.text = _text;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
