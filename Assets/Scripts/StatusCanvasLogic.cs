using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.U2D;

public class StatusCanvasLogic : MonoBehaviour
{
    public SpriteAtlas Sprites;

    private string _nameLabel; 
    public string NameLabel {
        get
        {
            return nameLabel.text;
        }
        set
        {
            _nameLabel = value;
            if (initiated)
            {
                nameLabel.text = value;
            }
        }
    }

    private int _maxHp;
    public int MaxHP
    {
        get
        {
            return (int)hpGauge.maxValue;
        }
        set
        {
            _maxHp = value;
            if (initiated)
            {
                hpGauge.maxValue = _maxHp;
                refleshHPText();
            }
        }
    }
    private int _currentHp;
    public int CurrentHP
    {
        get
        {
            return (int)hpGauge.value;
        }
        set
        {
            _currentHp = value;
            if (initiated)
            {
                hpGauge.value = _currentHp;
                refleshHPText();
            }
        }
    }
    private int _maxMp;
    public int MaxMP
    {
        get
        {
            return (int)mpGauge.maxValue;
        }
        set
        {
            _maxMp = value;
            if (initiated)
            {
                mpGauge.maxValue = _maxMp;
                refleshMPText();
            }
        }
    }
    private int _currentMp;
    public int CurrentMP
    {
        get
        {
            return (int)mpGauge.value;
        }
        set
        {
            _currentMp = value;
            if (initiated)
            {
                mpGauge.value = _currentMp;
                refleshMPText();
            }
        }
    }

    private int _currentInitiative;
    public int CurrentInitiative
    {
        get
        {
            return _currentInitiative;
        }
        set
        {
            _currentInitiative = value;
            if (initiated)
            {
                initiativeLabel.text = $"{_currentInitiative, 2}";
            }
        }
    }

    private TextMeshProUGUI nameLabel;
    private TextMeshProUGUI hpLabel;
    private Slider hpGauge;
    private TextMeshProUGUI mpLabel;
    private Slider mpGauge;
    private TextMeshProUGUI initiativeLabel;
    private Image statusIcon;

    private bool initiated = false;
    private bool effecting = false;

    // Start is called before the first frame update
    void Start()
    {
        nameLabel = gameObject.transform.Find("Name").gameObject.GetComponent<TextMeshProUGUI>();
        hpLabel = gameObject.transform.Find("HP").gameObject.GetComponent<TextMeshProUGUI>();
        hpGauge = gameObject.transform.Find("HPGauge").gameObject.GetComponent<Slider>();
        mpLabel = gameObject.transform.Find("MP").gameObject.GetComponent<TextMeshProUGUI>();
        mpGauge = gameObject.transform.Find("MPGauge").gameObject.GetComponent<Slider>();
        initiativeLabel = gameObject.transform.Find("Initiative").gameObject.GetComponent<TextMeshProUGUI>();
        statusIcon = gameObject.transform.Find("StatusIcon").gameObject.GetComponent<Image>();

        initiated = true;

        NameLabel = _nameLabel;
        MaxHP = _maxHp;
        CurrentHP = _currentHp;
        MaxMP = _maxMp;
        CurrentMP = _currentMp;
        initiativeLabel.text = "0";

        statusIcon.enabled = false;
        //statusIcon.sprite = Sprites.GetSprite("");
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void refleshHPText()
    {
        hpLabel.text = $"{_currentHp, 3}/{_maxHp, 3}";
        if(_currentHp <= 0)
        {
            //statusIcon.enabled = true;
            //statusIcon.sprite = Sprites.GetSprite("icon_dead");
            hpLabel.text = $" ��/{_maxHp, 3}";
        }
    }

    private void refleshMPText()
    {
        mpLabel.text = $"{_currentMp, 3}/{_maxMp, 3}";
    }

    public void DamageEffect()
    {
        if (!effecting)
        {
            effecting = true;
            StartCoroutine(damageEffectRoutine());
        }
        effecting = false;
    }

    private IEnumerator damageEffectRoutine()
    {
        var oldPos = transform.position;
        transform.position = new Vector3(oldPos.x - 10, oldPos.y - 2, oldPos.z);
        yield return null;
        transform.position = new Vector3(oldPos.x + 10, oldPos.y - 4, oldPos.z);
        yield return null;
        transform.position = new Vector3(oldPos.x - 10, oldPos.y - 6, oldPos.z);
        yield return null;
        transform.position = new Vector3(oldPos.x + 10, oldPos.y - 8, oldPos.z);
        yield return null;
        transform.position = new Vector3(oldPos.x, oldPos.y, oldPos.z);
    }
}
