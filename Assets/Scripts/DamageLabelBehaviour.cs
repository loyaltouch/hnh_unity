using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DamageLabelBehaviour : MonoBehaviour
{
    private TextMeshProUGUI label;
    private int _value;
    public int Value {
        get
        {
            return _value;
        }
        set
        {
            _value = value;
            if(label != null)
            {
                label.text = _value.ToString();
            }
        }
    }
    public int Direction { get; set; }

    public float Delay = 0.05f;

    // Start is called before the first frame update
    void Start()
    {
        label = gameObject.GetComponent<TextMeshProUGUI>();
        label.text = _value.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartEffect()
    {
        StartCoroutine(processEffect());
    }

    public IEnumerator processEffect()
    {
        var orgPos = transform.position;
        gameObject.SetActive(true);
        transform.position = new Vector3(orgPos.x, orgPos.y + 20 * Direction, 0);
        yield return new WaitForSeconds(Delay);
        transform.position = new Vector3(orgPos.x, orgPos.y + 10 * Direction, 0);
        yield return new WaitForSeconds(Delay);
        transform.position = new Vector3(orgPos.x, orgPos.y, 0);
        yield return new WaitForSeconds(Delay * 10);
        transform.position = new Vector3(orgPos.x, orgPos.y - 10 * Direction, 0);
        yield return new WaitForSeconds(Delay);
        transform.position = new Vector3(orgPos.x, orgPos.y - 20 * Direction, 0);
        yield return new WaitForSeconds(Delay);
        Destroy(gameObject);
    }
}
