using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;
using TMPro;

public class SelectionIconBehavior : MonoBehaviour
{
    private Image selectIconImage;
    private TextMeshProUGUI selectIconLabel;
    private SpriteAtlas atlas;

    private string _image;
    private string _label;
    private bool _selected;
    private bool _enabled;
    private bool _effecting = false;

    public string Image { 
        get
        {
            return _image;
        }
        set
        {
            _image = value;
            if(selectIconImage != null && atlas != null)
            {
                selectIconImage.sprite = atlas.GetSprite(_image);
            }
            _enabled = (_image == "0");
        }
    }

    public string Label
    {
        get
        {
            return _label;
        }
        set
        {
            _label = value;
            if(selectIconLabel != null)
            {
                selectIconLabel.text = _label;
            }
        }
    }

    public bool Selected
    {
        get
        {
            return _selected;
        }
        set
        {
            _selected = value;
            refleshImage();
        }
    }

    public bool Enabled
    {
        get
        {
            return _enabled;
        }
        set
        {
            _enabled = value;
            refleshImage();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        selectIconImage = transform.Find("SelectIconImage").gameObject.GetComponent<Image>();
        selectIconLabel = transform.Find("SelectIconLabel").gameObject.GetComponent<TextMeshProUGUI>();
        atlas = Resources.Load<SpriteAtlas>("Images/Atlas");

        selectIconImage.sprite = atlas.GetSprite(_image);
        selectIconLabel.text = _label;
        _enabled = true;
        refleshImage();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void refleshImage()
    {
        if(selectIconImage != null)
        {
            gameObject.SetActive(_enabled);

            if (_selected)
            {
                //selectIconImage.color = new Color(1, 1, 1, 1);
                selectIconImage.transform.localScale = new Vector3(1.2f, 1.2f, 1);
            }
            else
            {
                //selectIconImage.color = new Color(1, 1, 1, 1);
                selectIconImage.transform.localScale = new Vector3(1, 1, 1);
            }
        }
    }

    public void DamageEffect()
    {
        if (!_effecting)
        {
            _effecting = true;
            StartCoroutine(damageEffectRoutine());
        }
        _effecting = false;
    }

    private IEnumerator damageEffectRoutine()
    {
        var oldPos = transform.position;
        transform.position = new Vector3(oldPos.x - 10, oldPos.y - 2, oldPos.z);
        yield return null;
        transform.position = new Vector3(oldPos.x + 10, oldPos.y - 4, oldPos.z);
        yield return null;
        transform.position = new Vector3(oldPos.x - 10, oldPos.y - 6, oldPos.z);
        yield return null;
        transform.position = new Vector3(oldPos.x + 10, oldPos.y - 8, oldPos.z);
        yield return null;
        transform.position = new Vector3(oldPos.x, oldPos.y, oldPos.z);
    }
}
