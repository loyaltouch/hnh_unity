using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;
using TMPro;
using Assets.Scripts;

public class MainLogic : MonoBehaviour
{
    public GameObject canvas;
    public GameObject selectionIconCanvas;
    public GameObject InputField;
    public TextMeshProUGUI title;
    public TextMeshProUGUI message;
    public GameObject statusPanel;
    public Image blinkImage;
    public Image iconImage;
    public RawImage bgImage;
    public SpriteAtlas atlas;

    private DomainLogic dl = new DomainLogic();
    private GameObject selectButtonPrefab;
    private GameObject statusPrefab;
    private GameObject selectIconPrefab;
    private GameObject damageLablPrefab;
    private List<GameObject> selectButtons = new List<GameObject>();
    private List<GameObject> selectIcons = new List<GameObject>();
    private List<string> playerList;
    private Dictionary<string, GameObject> optionButtons;
    private bool selectAnimating = false;
    private int selectIndex = 0;

    private Dictionary<string, StatusCanvasLogic> statusData = new Dictionary<string, StatusCanvasLogic>();


    // Start is called before the first frame update
    void Start()
    {
        loadMemberData();
        loadScenario("Scene1/bar");

        playerList = setupPlayerList();
        foreach (var player in playerList)
        {
            dl.Inn(player);
            updatePlayerStatus(player);
        }

        selectButtonPrefab = Resources.Load<GameObject>("Prefabs/Button");
        optionButtons = setupOptionButtons();

        selectIconPrefab = Resources.Load<GameObject>("Prefabs/SelectIcon");

        doSelection("start");

        damageLablPrefab = Resources.Load<GameObject>("Prefabs/DamageLabel");
    }

    // Update is called once per frame
    void Update()
    {
        if (!selectAnimating)
        {
            if(selectButtons.Count > 0)
            {
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    if (selectIndex > 0)
                    {
                        StartCoroutine(moveSelectButton(-1));
                    }
                    else
                    {
                        StartCoroutine(moveSelectButton(selectButtons.Count - 1));
                    }
                }
                if (Input.GetKey(KeyCode.DownArrow))
                {
                    if (selectIndex < selectButtons.Count - 1)
                    {
                        StartCoroutine(moveSelectButton(1));
                    }
                    else
                    {
                        StartCoroutine(moveSelectButton((selectButtons.Count - 1) * -1));
                    }
                }
            }
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
            {
                doSelection(dl.GetGameData("select", selectIndex.ToString()));
            }
        }
    }

    public void OnClickButton()
    {
        doSelection(dl.GetGameData("select", selectIndex.ToString()));
    }

    private void doSelection(string select)
    {
        dl.SetGameData("system", "selected", select);
        var result = dl.parse(select.Replace(".", " "));
        var loadScene = dl.GetGameData("scene", "load");
        if(loadScene != "0")
        {
            loadScenario(loadScene);
        }
        dl.SetupScene();
        dl.Invoke(result);
        updateMessage();
        updateTitle();
        updateIcon(dl.GetGameData("view", "icon"));
        updateBackGround(dl.GetGameData("view", "background"));
        foreach (var player in playerList)
        {
            updatePlayerStatus(player);
        }
        updateSelections();
        updateSelectIcon();
        updateInputField();
        updateOptionMenu();
    }


    private void updateMessage()
    {
        message.text = dl.GetGameData("view", "msg").Replace('.', '\n');
    }

    private void updateTitle()
    {
        title.text = dl.GetGameData("view", "title");
    }

    private void updatePlayerStatus(string player)
    {
        var sl = statusData[player].GetComponent<StatusCanvasLogic>();
        sl.NameLabel = player;
        if (dl.GetGameData($"members.{player}.status", "dead") != "0")
        {
        }
        sl.MaxHP = dl.GetGameDataInt($"members.{player}", "HP");
        sl.CurrentHP = dl.GetGameDataInt($"members.{player}", "HPnow");
        sl.MaxMP = dl.GetGameDataInt($"members.{player}", "MP");
        sl.CurrentMP = dl.GetGameDataInt($"members.{player}", "MPnow");
        sl.CurrentInitiative = dl.GetGameDataInt($"members.{player}", "initiative");

        var effected = dl.GetGameData("scene", "damageEffect");
        if(player == effected)
        {
            sl.DamageEffect();
            var damageLabel = Instantiate<GameObject>(damageLablPrefab, statusData[player].transform).GetComponent<DamageLabelBehaviour>();
            damageLabel.Value = dl.GetGameDataInt("scene", "effectValue");
            damageLabel.Direction = 1;
            damageLabel.StartEffect();
        }
        var cureEffected = dl.GetGameData("scene", "cureEffect");
        if(player == cureEffected)
        {
            var damageLabel = Instantiate<GameObject>(damageLablPrefab, statusData[player].transform).GetComponent<DamageLabelBehaviour>();
            damageLabel.Value = dl.GetGameDataInt("scene", "effectValue");
            damageLabel.Direction = -1;
            damageLabel.StartEffect();
        }
    }


    private List<string> setupPlayerList()
    {
        statusPrefab = GameObject.Find("StatusCanvas");

        statusData["戦士"] = statusPrefab.GetComponent<StatusCanvasLogic>();
        statusData["戦士"].Sprites = atlas;

        var status = Instantiate<GameObject>(statusPrefab, statusPanel.transform);
        status.transform.localPosition = new Vector3(0, statusPrefab.transform.localPosition.y - 30, 0);
        status.transform.Find("CharaImage").GetComponent<Image>().sprite = atlas.GetSprite("priest_blink0");
        statusData["僧侶"] = status.GetComponent<StatusCanvasLogic>();
        statusData["僧侶"].Sprites = atlas;

        status = Instantiate<GameObject>(statusPrefab, statusPanel.transform);
        status.transform.localPosition = new Vector3(0, statusPrefab.transform.localPosition.y - 60, 0);
        status.transform.Find("CharaImage").GetComponent<Image>().sprite = atlas.GetSprite("magusknight_blink0");
        statusData["魔法剣士"] = status.GetComponent<StatusCanvasLogic>();
        statusData["魔法剣士"].Sprites = atlas;

        status = Instantiate<GameObject>(statusPrefab, statusPanel.transform);
        status.transform.localPosition = new Vector3(400, statusPrefab.transform.localPosition.y - 0, 0);
        status.transform.Find("CharaImage").GetComponent<Image>().sprite = atlas.GetSprite("magicuser_blink0");
        statusData["魔術師"] = status.GetComponent<StatusCanvasLogic>();
        statusData["魔術師"].Sprites = atlas;

        status = Instantiate<GameObject>(statusPrefab, statusPanel.transform);
        status.transform.localPosition = new Vector3(400, statusPrefab.transform.localPosition.y - 30, 0);
        status.transform.Find("CharaImage").GetComponent<Image>().sprite = atlas.GetSprite("thief_blink0");
        statusData["盗賊"] = status.GetComponent<StatusCanvasLogic>();
        statusData["盗賊"].Sprites = atlas;

        // アイコンをグレースケールにする方法
        //statusPrefab.transform.Find("CharaImage").GetComponent<Image>().material = Resources.Load<Material>("Images/GrayScale");
        // アイコンを瞬きさせる
        //StartCoroutine(blinker(statusPrefab.transform.Find("CharaImage").gameObject));

        var playerList = new List<string>(){
            "戦士", "僧侶", "魔法剣士", "魔術師", "盗賊"
        };
        return playerList;
    }

    private Dictionary<string, GameObject> setupOptionButtons()
    {
        var result = new Dictionary<string, GameObject>();
        for(var i = 1; i <= 5; i++)
        {
            result[$"F{i}Text"] = GameObject.Find($"F{i}Text");
        }
        return result;
    }


    private void updateSelections()
    {
        // 前画面の選択ボタンをクリア
        foreach(var selectButton in selectButtons)
        {
            Destroy(selectButton);
        }
        selectButtons.Clear();

        // DomainLogicから選択ボタンを構築
        for(var i = 0; i < dl.GetGameDataInt("select", "count"); i++)
        {
            var selectButton = Instantiate<GameObject>(selectButtonPrefab, canvas.transform);
            selectButton.transform.localPosition = new Vector3(0f, -35.0f * i, 0f);
            var label = selectButton.transform.Find("ButtonText").gameObject.GetComponent<TextMeshProUGUI>();
            label.text = dl.GetGameData($"select.{i}", "label");
            selectButtons.Add(selectButton);
        }

        selectIndex = 0;
    }

    private void updateIcon(string iconName)
    {
        iconImage.gameObject.SetActive(iconName != "0");
        var image = atlas.GetSprite(iconName);
        if (image != null)
        {
            iconImage.sprite = image;
        }
        var isBlink = dl.GetGameData($"blinker.{iconName}", "name") != "0";
        blinkImage.gameObject.SetActive(isBlink);
        if(isBlink)
        {
            blinkImage.transform.localPosition = new Vector3(
                dl.GetGameDataInt($"blinker.{iconName}", "x"),
                dl.GetGameDataInt($"blinker.{iconName}", "y"),
                0
                );
        }
    }

    private void updateBackGround(string bgName)
    {
        var image = Resources.Load<Texture>($"Images/{bgName}");
        if (image != null)
        {
            bgImage.texture = image;
        }
    }

    private void updateOptionMenu()
    {
        for(var i = 1; i <= 5; i++)
        {
            var txt = optionButtons[$"F{i}Text"].GetComponent<TextMeshProUGUI>();
            var label = dl.GetGameData("view", $"f{i}");
            if(label != "0")
            {
                txt.text = $"F{i} {label}";
            }
            else
            {
                txt.text = $"F{i}";
            }
        }
    }

    private void updateSelectIcon()
    {
        // 全てのアイコン削除
        foreach(Transform trans in selectionIconCanvas.transform)
        {
            Destroy(trans.gameObject);
        }

        for(var i = 0; i < dl.GetGameDataInt("selecticon", "count"); i++)
        {
            var iconImage = dl.GetGameData($"selecticon.{i}", "image");
            if(iconImage != "0")
            {
                var icon = Instantiate(selectIconPrefab, selectionIconCanvas.transform);
                icon.transform.localPosition = new Vector3(100 + (i % 3) * 80, 100 + (i / 3) - (i % 3) * 10, 0);
                var selicon = icon.GetComponent<SelectionIconBehavior>();
                selicon.Label = dl.GetGameData($"selecticon.{i}", "label");
                selicon.Image = iconImage;

                // damag effect
                if(selicon.Label == dl.GetGameData("scene", "damageEffect"))
                {
                    selicon.DamageEffect();
                    var damageLabel = Instantiate<GameObject>(damageLablPrefab, icon.transform).GetComponent<DamageLabelBehaviour>();
                    damageLabel.Value = dl.GetGameDataInt("scene", "effectValue");
                    damageLabel.Direction = 1;
                    damageLabel.StartEffect();
                }
                if(selicon.Label == dl.GetGameData("scene", "cureEffect"))
                {
                    var damageLabel = Instantiate<GameObject>(damageLablPrefab, icon.transform).GetComponent<DamageLabelBehaviour>();
                    damageLabel.Value = dl.GetGameDataInt("scene", "effectValue");
                    damageLabel.Direction = -1;
                    damageLabel.StartEffect();
                }
            }
        }
    }

    private void updateInputField()
    {
        InputField.GetComponent<InputFieldBehavior>().Text = "";
        InputField.SetActive(dl.GetGameData("select", "inputfield") != "0");
     }

    IEnumerator moveSelectButton(int direction)
    {
        selectAnimating = true;
        var oldY = new Dictionary<GameObject, float>();
        var newY = new Dictionary<GameObject, float>();
        foreach(var button in selectButtons)
        {
            oldY[button] = button.transform.position.y;
            newY[button] = button.transform.position.y + 35 * direction;
        }
        for(float f = 0; f < 0.25f; f += Time.deltaTime)
        {
            foreach (var button in selectButtons)
            {
                button.transform.position = Vector3.Lerp(button.transform.position, new Vector3(button.transform.position.x, newY[button], button.transform.position.z), f * 4);
            }
            yield return null;
        }
        foreach (var button in selectButtons)
        {
            button.transform.position =new Vector3(button.transform.position.x, newY[button], button.transform.position.z);
        }
        selectAnimating = false;
        selectIndex += direction;
    }

    private IEnumerator blinker(GameObject icon)
    {
        var iconImage = icon.GetComponent<Image>();
        for(; ; )
        {
            icon.transform.localPosition = new Vector3(-330, 0, 0);
            icon.transform.localScale = new Vector3(1, 1, 1);
            icon.GetComponent<RectTransform>().sizeDelta = new Vector2(100, 30);
            iconImage.sprite = atlas.GetSprite("fighter_blink0");
            yield return new WaitForSeconds(Random.Range(1, 5));

            iconImage.sprite = atlas.GetSprite("fighter_blink1");
            yield return new WaitForSeconds(0.1f);

            iconImage.sprite = atlas.GetSprite("fighter_blink2");
            yield return new WaitForSeconds(0.1f);

            iconImage.sprite = atlas.GetSprite("fighter_blink1");
            yield return new WaitForSeconds(0.1f);

        }
    }

    private void loadMemberData()
    {
        var txt = Resources.Load<TextAsset>("Data/members");
        dl.ParseResourceData("members", txt.text);

        dl.parse("戦士.僧侶.魔法剣士 group.-1 :newlist");
        dl.parse("盗賊.魔術師 group.-2 :newlist");
    }

    private void loadScenario(string path)
    {
        var txt = Resources.Load<TextAsset>($"Scenario/{path}");
        dl.ParseScenario(txt.text);
    }

    private void loadBlinkData()
    {
        var txt = Resources.Load<TextAsset>("Data/blinkeer");
        dl.ParseResourceData("blinker", txt.text);
    }
}
