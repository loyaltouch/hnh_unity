Shader "Unlit/ImageGrayScale"
{
    Properties
    {
        [PerRendererData] _MainTex("Main Tex", 2D) = "white" {}
        _Color("Tint", Color) = (1,1,1,1)
        [MaterialToggle] PixelSnap("Pixel snap", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"
            "RenderType" = "Transparent"
            "PreviewType" = "Plane"
            "CanUseSpriteAtlas" = "True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        Pass
        {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            // 追加
            #pragma multi_compile __ UNITY_UI_CLIP_RECT

            #include "UnityCG.cginc"
            // 追加
            #include "UnityUI.cginc"

            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color : COLOR;
                half2 texcoord  : TEXCOORD0;
                float4 worldPosition : TEXCOORD1; // 追加
            };

            sampler2D _MainTex;

            fixed4 Grayscale(fixed4 color)
            {
                return dot(color.rgb, float3(0.3, 0.59, 0.11)) * color.a;
                //return dot(color.rgb, float3(0.5, 0.5, 0.5)) * color.a;
            }

            v2f vert(appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.texcoord = v.texcoord;
                o.worldPosition = v.vertex; // 追加
                o.color = v.color;

#ifdef PIXELSNAP_ON
                o.vertex = UnityPixelSnap(o.vertex);
#endif

                return o;
            }

            //sampler2D _MainTex;
            sampler2D _AlphaTex;
            float4 _ClipRect; // 追加
            fixed4 SampleSpriteTexture(float2 uv)
            {
                fixed4 color = tex2D(_MainTex, uv);

#if ETC1_EXTERNAL_ALPHA
                // get the color from an external texture (usecase: Alpha support for ETC1 on android)
                color.a = tex2D(_AlphaTex, uv).r;
#endif //ETC1_EXTERNAL_ALPHA

                return color;
            }


            fixed4 frag(v2f i) : SV_Target
            {
                //fixed4 tex = tex2D(_MainTex, i.texcoord) * i.color;
                fixed4 tex = SampleSpriteTexture(i.texcoord) * i.color;

            // 追加
#ifdef UNITY_UI_CLIP_RECT
            tex.a *= UnityGet2DClipping(i.worldPosition.xy, _ClipRect);
            clip(tex.a - 0.001);
#endif //UNITY_UI_CLIP_RECT

                tex.rgb = Grayscale(tex);
                return tex;
            }

        ENDCG
        }
    }
}
