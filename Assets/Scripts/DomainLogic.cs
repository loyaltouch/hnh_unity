﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Assets.Scripts
{
    public class DomainLogic
    {
        private Random random = new Random();
        public Dictionary<string, string> Func { get; set; }
        public string Dump { get; set; }
        private Dictionary<string, Dictionary<string, string>> gameData = new Dictionary<string, Dictionary<string, string>>();

        public DomainLogic()
        {
            Func = new Dictionary<string, string>();
        }

        public bool Invoke(string func)
        {
            var text = "";
            var ret = Func.TryGetValue(func, out text);
            if (ret)
            {
                parse(text);
            }
            return ret;
        }

        public string parse(string text)
        {
            var tokens = Regex.Split(text, " +");
            var stack = new Stack<string>();
            var skip = false;
            stack.Push("0");
            for(var i = 0; i < tokens.Length; i++)
            {
                if(tokens[i] == "\n")
                {
                    Dump = "";
                    foreach (var token in stack)
                    {
                        Dump += token + "\n";
                    }
                    skip = false;
                }else if (!skip)
                {
                    switch (tokens[i])
                    {
                        case "+":
                            stack.Push(((atoi(stack.Pop()) + atoi(stack.Pop()))).ToString());
                            break;
                        case "-":
                            stack.Push(((atoi(stack.Pop()) - atoi(stack.Pop()))).ToString());
                            break;
                        case "*":
                            stack.Push(((atoi(stack.Pop()) * atoi(stack.Pop()))).ToString());
                            break;
                        case "/":
                            stack.Push(((atoi(stack.Pop()) / atoi(stack.Pop()))).ToString());
                            break;
                        case "%":
                            stack.Push(((atoi(stack.Pop()) % atoi(stack.Pop()))).ToString());
                            break;
                        case ">":
                            stack.Push((atoi(stack.Pop()) > atoi(stack.Pop())) ? "1" : "0");
                            break;
                        case "<":
                            stack.Push((atoi(stack.Pop()) < atoi(stack.Pop())) ? "1" : "0");
                            break;
                        case "==":
                            stack.Push((atoi(stack.Pop()) == atoi(stack.Pop())) ? "1" : "0");
                            break;
                        case "$":
                            stack.Push(GetGameData(stack.Pop(), stack.Pop()));
                            break;
                        case ":=":
                            SetGameData(stack.Pop(), stack.Pop(), stack.Pop());
                            break;
                        case ".":
                            stack.Push($"{stack.Pop()}.{stack.Pop()}");
                            break;
                        case ";":
                            concat(stack, ".");
                            break;
                        case ":&":
                            concat(stack, "");
                            break;
                        case ":break":
                            return stack.Pop();
                        case ":cure":
                            macro_cure(stack);
                            break;
                        case ":clearKey":
                            ClearGameDataKey(stack.Pop());
                            break;
                        case ":damage":
                            macro_damage(stack);
                            break;
                        case ":debug":
                            Dump = string.Join("\n", stack.ToArray<string>());
                            break;
                        case ":encounter":
                            macro_encounter(stack);
                            break;
                        case ":eq":
                            stack.Push((stack.Pop() == stack.Pop()) ? "1" : "0");
                            break;
                        case ":filter":
                            macro_filter(stack);
                            break;
                        case ":find":
                            macro_find(stack);
                            break;
                        case ":if":
                            skip = (stack.Pop() == "0");
                            break;
                        case ":initiative":
                            InitInitiative(GetList("buttleMember"));
                            break;
                        case ":inittop":
                            stack.Push(macro_inittop());
                            break;
                        case ":listselect":
                            macro_listselect(stack);
                            break;
                        case ":livemembers":
                            macro_livemembers(stack);
                            break;
                        case ":load":
                            Invoke(stack.Pop());
                            break;
                        case ":newlist":
                            macro_newlist(stack.Pop(), stack.Pop());
                            break;
                        case ":not":
                            stack.Push(stack.Pop() == "0" ? "1" : "0");
                            break;
                        case ":random":
                            stack.Push(random.Next(0, atoi(stack.Pop())).ToString());
                            break;
                        case ":sel":
                            macro_sel(stack);
                            break;
                        case ":setupgroups":
                            macro_setupgroups(stack);
                            break;
                        case ":stat":
                            stack.Push(GetStatusInt(stack.Pop(), stack.Pop()).ToString());
                            break;
                        case ":tolist":
                            stack.Push(string.Join(".", GetList(stack.Pop())));
                            break;
                        case ":topinit":
                            macro_topInitiative();
                            break;
                        default:
                            stack.Push(tokens[i]);
                            break;
                    }
                }
            }
            return stack.Pop();
        }

        private void concat(Stack<string> stack, string delimiter)
        {
            var lines = GetGameData("scene", "lines");
            if(lines == "0")
            {
                lines = stack.Pop();
            }
            else
            {
                lines += $"{delimiter}{stack.Pop()}";
            }
            SetGameData("scene", "lines", lines);
        }

        private void macro_sel(Stack<string> stack)
        {
            var selectIndex = GetGameDataInt("select", "count");
            SetGameData("select", selectIndex.ToString(), stack.Peek());
            SetGameData($"select.{selectIndex}", "value", stack.Pop());
            SetGameData($"select.{selectIndex}", "label", stack.Pop());
            SetGameData("select", "count", (selectIndex + 1).ToString());
        }

        private void macro_topInitiative()
        {
            var maxInit = 0;
            var maxMember = "0";
            foreach(var member in GetList("buttleMember"))
            {
                var init = GetGameDataInt($"members.{member}", "initiative");
                if(init > maxInit)
                {
                    maxInit = init;
                    maxMember = member;
                }
            }
            SetGameData("action", "actor", maxMember);
        }

        private string macro_inittop()
        {
            var maxInit = 0;
            var maxMember = "0";
            foreach (var member in GetList("buttleMember"))
            {
                var init = GetGameDataInt($"members.{member}", "initiative");
                if (init > maxInit)
                {
                    maxInit = init;
                    maxMember = member;
                }
            }
            return maxMember;
        }

        private void macro_newlist(string key, string listStr)
        {
            var listItems = listStr.Split('.');
            SetGameData(key, "count", listItems.Length.ToString());
            for(var i = 0; i < listItems.Length; i++)
            {
                SetGameData(key, i.ToString(), listItems[i]);
            }
        }

        private void macro_addlist(string key, string value)
        {
            var count = GetGameDataInt(key, "count");
            SetGameData(key, "count", (count + 1).ToString());
            SetGameData(key, count.ToString(), value);
        }

        private void macro_damage(Stack<string> stack)
        {
            var key = stack.Pop();
            var name = stack.Pop();
            var value = atoi(stack.Pop());
            if(value < 0)
            {
                value = 0;
            }
            var current = GetGameDataInt($"members.{key}", $"{name}now");
            SetGameData($"members.{key}", $"{name}now", (current - value).ToString());
            stack.Push(value.ToString());
        }

        private void macro_cure(Stack<string> stack)
        {
            var key = stack.Pop();
            var name = stack.Pop();
            var value = atoi(stack.Pop());
            if (value < 0)
            {
                value = 0;
            }
            var current = GetGameDataInt($"members.{key}", $"{name}now");
            var max = GetGameDataInt($"members.{key}", name);
            if(current + value > max)
            {
                value = max - current;
            }
            SetGameData($"members.{key}", $"{name}now", (current + value).ToString());
            stack.Push(value.ToString());
        }

        private void macro_encounter(Stack<string> stack)
        {
            var dataName = stack.Pop();
            var instanceName = stack.Pop();
            var group = stack.Pop();

            //  ステータスデータのコピー
            var statusList = new string[] { "name", "vit", "dex", "men", "HP", "MP", "icon", "algo"};
            foreach(var statusName in statusList)
            {
                SetGameData($"members.{instanceName}", statusName, GetGameData($"members.{dataName}", statusName));
            }
            // HP,MPの初期化
            Inn(instanceName);

            // ステータスの初期化
            ClearGameDataKey($"members.{instanceName}.status");

            // 戦闘メンバーに参加
            macro_addlist("buttleMember", instanceName);

            // 隊列に編入
            macro_addlist($"group.{group}", instanceName);

            // アイコンを追加
            var iconIndex = GetGameData("selecticon", "count");
            SetGameData($"selecticon.{iconIndex}", "label", instanceName);
            SetGameData($"selecticon.{iconIndex}", "image", GetGameData($"members.{instanceName}", "icon"));
            macro_addlist("selecticon", instanceName);
        }

        private void macro_setupgroups(Stack<string> stack)
        {
            // 味方グループの並び替え
            var livePerty1 = liveMembers(GetList("group.-1"));
            if(livePerty1.Count <= 0)
            {
                macro_newlist("group.-1", string.Join(".", GetList("group.-2")));
                SetGameData("group.-2", "count", "0");
            }
        }

        /**
         * 
         */
        private void macro_filter(Stack<string> stack)
        {
            var tokens = stack.Pop().Replace(".", " ");
            var pattern = stack.Pop();

            var result = new List<string>();
            var keys = new string[gameData.Keys.Count];
            gameData.Keys.CopyTo(keys, 0);
            foreach(var key in keys)
            {
                if(Regex.IsMatch(key, pattern))
                {
                    var subkeys = new string[gameData[key].Count];
                    gameData[key].Keys.CopyTo(subkeys, 0);
                    foreach(var subkey in subkeys)
                    {
                        var value = GetGameData(key, subkey);
                        SetGameData("system", "filtervalue", value);
                        var parseResult = parse(tokens);
                        if(parseResult != "0")
                        {
                            result.Add(subkey);
                        }
                    }
                }
            }
        }

        private void macro_livemembers(Stack<string> stack)
        {
            var members = stack.Pop().Split('.');
            var result = liveMembers(new List<string>(members));
            if(result.Count > 0)
            {
                stack.Push(string.Join(".", result));
            }
            else
            {
                stack.Push("0");
            }
        }

        private List<string> liveMembers(List<string> members)
        {
            var result = new List<string>();
            foreach(var member in members)
            {
                if (GetGameData($"members.{member}.status", "dead") == "0")
                {
                    result.Add(member);
                }
            }
            return result;
        }

        private List<string> getLiveMembers(List<string> members)
        {
            var result = new List<string>();
            return result;
        }

        private void macro_listselect(Stack<string> stack)
        {
            var option = stack.Pop();
            var group = stack.Pop().Split('.');
            foreach(var entry in group)
            {
                stack.Push(entry);
                stack.Push($"{entry}.{option}");
                macro_sel(stack);
            }
        }

        private void macro_find(Stack<string> stack)
        {
            var listName = stack.Pop();
            var key = stack.Pop();
            try
            {
                var list = gameData[listName];
                var result = list.FirstOrDefault(entry => entry.Value == key);
                stack.Push(result.Key);
                SetGameData("system", "lasterror", "0");
            }
            catch (KeyNotFoundException)
            {
                SetGameData("system", "lasterror", "nolist");
                stack.Push("0");
            }
        }

        public string GetGameData(string key, string name)
        {
            try
            {
                return gameData[key][name];
            }
            catch (KeyNotFoundException)
            {
                return "0";
            }
        }

        public int GetGameDataInt(string key, string name)
        {
            return atoi(GetGameData(key, name));
        }

        public void SetGameData(string key, string name, string value)
        {
            if (!gameData.ContainsKey(key))
            {
                gameData[key] = new Dictionary<string, string>();
            }
            gameData[key][name] = value;
        }

        public void ClearGameDataKey(string key)
        {
            gameData[key] = new Dictionary<string, string>();
        }

        public void SetupScene()
        {
            ClearGameDataKey("scene");
            SetGameData("select", "count", "0");
            for(var i = 1; i <= 5; i++)
            {
                SetGameData("select", $"f{i}", "0");
                SetGameData("view", $"f{i}", "0");
            }
            SetGameData("select", "inputfield", "0");
        }

        public void ParseResourceData(string resourceName, string dataStr)
        {
            string[] headers = null;
            var lines = dataStr.Split('\n');
            for(var i = 0; i < lines.Length; i++)
            {
                var line = lines[i].Replace("\r", "");
                if(headers == null)
                {
                    headers = line.Split(',');
                }
                else
                {
                    var columns = line.Split(',');
                    for (var j = 0; j < columns.Length; j++)
                    {
                        SetGameData($"{resourceName}.{columns[0]}", headers[j], columns[j]);
                    }
                }
            }
        }

        public void Inn(string name)
        {
            SetGameData($"members.{name}", "HPnow", GetGameData($"members.{name}", "HP"));
            SetGameData($"members.{name}", "MPnow", GetGameData($"members.{name}", "MP"));
        }

        public void InitInitiative(List<string> buttleMember)
        {
            foreach(var member in buttleMember)
       
            {
                if(GetGameData($"members.{member}.status", "dead") == "0")
                {
                    var nowDex = GetStatusInt(member, "dex");
                    var init = nowDex + rand();
                    SetGameData($"members.{member}", "initiative", init.ToString());
                }
            }
        }

        public int GetStatusInt(string name, string status)
        {
            if(status == "dex")
            {
                return GetGameDataInt($"members.{name}", "dex");
            }
            if(status == "atk")
            {
                return GetGameDataInt($"members.{name}", "vit") / 4 + 1;
            }
            return 0;
        }

        public List<string> GetList(string key)
        {
            var result = new List<string>();
            for(var i = 0; i < GetGameDataInt(key, "count"); i++)
            {
                result.Add(GetGameData(key, i.ToString()));
            }
            return result;
        }

        private int rand()
        {
            return random.Next(1, 6) + random.Next(1, 6);
        }

        public void ParseScenario(string scenario)
        {
            var cache = "";
            var lines = scenario.Split('\n');
            foreach(var line in lines)
            {
                var text = line.Replace("\r", "");
                var m = Regex.Match(text, @"^\&(.+)$");
                if (m.Success)
                {
                    var key = m.Groups[1].Value;
                    Func[key] = cache;
                    cache = "";
                }
                else
                {
                    cache += text + " \n ";
                }
            }
        }

        private int atoi(string value)
        {
            var result = 0;
            int.TryParse(value, out result);
            return result;
        }
    }
}
