using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ButtonBehaviour : MonoBehaviour
{
    TextMeshProUGUI childText;
    Image image;
    public float CanvasWidth = 256.0f;
    public float dist;
    public float blend;

    // Start is called before the first frame update
    void Start()
    {
        childText = transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>();
        image = GetComponent<Image>();
        childText.color = new Color(0, 0, 0, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        dist = Mathf.Abs(transform.localPosition.y);
        blend = (CanvasWidth - dist) / CanvasWidth;
        if(blend < 0)
        {
            blend = 0;
        }
        float solidColor = 0.5f;
        if (transform.localPosition.y == 0)
        {
            solidColor = 0f;
        }
        childText.color = new Color(solidColor, solidColor, solidColor, blend);
    }
}
