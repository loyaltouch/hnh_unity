using NUnit.Framework;
using Assert = UnityEngine.Assertions.Assert;
using Assets.Scripts;
using System.Linq;

public class testDomainLogic
{
    string _scenario = @"酒場のマスター title view :=
今のところ仕事はこれだけだよ msg view :=
barmaster icon view :=
bar background view :=
仕事を探す start :sel ゴブリン退治 gobsla :sel 下水掃除 cleaner :sel
&start";

    string _if = @"flag scene $ :if 1 set scene :=
&if";

    string _members = @"name,class,vit,dex,men,HP,MP,icon
戦士,戦士,12,9,7,20,7,fighter
僧侶,僧侶,10,7,10,16,10,priest
魔法剣士,魔法剣士,12,9,7,18,7,magusknight
魔術師,魔術師,8,4,12,8,16,magicuser
盗賊,盗賊,8,12,6,10,6,thief";

    [Test]
    public void testSelector()
    {
        var dl = new DomainLogic();
        dl.ParseScenario(_scenario);
        dl.Invoke("start");
        Assert.AreEqual("3", dl.GetGameData("select", "count"), "selectcount");
        Assert.AreEqual("start", dl.GetGameData("select", "0"), "select 0");
        Assert.AreEqual("start", dl.GetGameData("select.0", "value"), "select 0");
        Assert.AreEqual("仕事を探す", dl.GetGameData("select.0", "label"), "select 0");
        Assert.AreEqual("gobsla", dl.GetGameData("select", "1"), "select 1");
        Assert.AreEqual("gobsla", dl.GetGameData("select.1", "value"), "select 1");
        Assert.AreEqual("ゴブリン退治", dl.GetGameData("select.1", "label"), "select 1");
        Assert.AreEqual("cleaner", dl.GetGameData("select", "2"), "select 2");
        Assert.AreEqual("cleaner", dl.GetGameData("select.2", "value"), "select 2");
        Assert.AreEqual("下水掃除", dl.GetGameData("select.2", "label"), "select 2");
    }

    [TestCase("0", "0")]
    [TestCase("1", "1")]
    [TestCase("2", "1")]
    public void testIf(string arg, string result)
    {
        var dl = new DomainLogic();
        dl.ParseScenario(_if);
        dl.SetGameData("scene", "flag", arg);
        dl.Invoke("if");
        Assert.AreEqual(result, dl.GetGameData("scene", "set"));
    }

    [Test]
    public void testInitiative()
    {
        var dl = new DomainLogic();
        dl.ParseResourceData("members", _members);
        dl.SetGameData("members.盗賊", "dex", "120");
        dl.parse("戦士.僧侶.魔法剣士.魔術師.盗賊 buttleMember :newlist :initiative :topinit");
        Assert.IsTrue(dl.GetGameDataInt("members.戦士", "initiative") > 10, $"戦士={dl.GetGameDataInt("members.戦士", "initiative")} > 10");
        Assert.IsTrue(dl.GetGameDataInt("members.僧侶", "initiative") > 8, $"僧侶={dl.GetGameDataInt("members.僧侶", "initiative")} > 8");
        Assert.IsTrue(dl.GetGameDataInt("members.魔法剣士", "initiative") > 10, $"魔法剣士={dl.GetGameDataInt("members.魔法剣士", "initiative")} > 10");
        Assert.IsTrue(dl.GetGameDataInt("members.魔術師", "initiative") > 5, $"魔術師={dl.GetGameDataInt("members.魔術師", "initiative")} > 5");
        Assert.IsTrue(dl.GetGameDataInt("members.盗賊", "initiative") > 121, $"盗賊={dl.GetGameDataInt("members.盗賊", "initiative")} > 13");
        Assert.AreEqual("盗賊", dl.GetGameData("action", "actor"), $"actor={dl.GetGameData("action", "actor")}");
    }

    [Test]
    public void testShowMember()
    {
        var dl = new DomainLogic();
        dl.ParseResourceData("members", _members);
        dl.SetGameData("action", "actor", "僧侶");
        var name = dl.parse("actor action $");
        Assert.AreEqual("僧侶", name);
        var icon = dl.parse("icon actor action $ members . $");
        Assert.AreEqual("priest", icon);
    }

    [TestCase("戦士", "HP", "2", "2", "18")]
    [TestCase("魔術師", "HP", "5", "5", "3")]
    [TestCase("僧侶", "MP", "-2", "0", "10")]
    public void testDamage(string name, string key, string value, string result, string remain)
    {
        var dl = new DomainLogic();
        dl.ParseResourceData("members", _members);
        dl.Inn(name);
        var result2 = dl.parse($"{value} {key} {name} :damage");
        Assert.AreEqual(result, result2, "result");
        Assert.AreEqual(remain, dl.GetGameData($"members.{name}", $"{key}now"), "remain");
    }

    [TestCase("戦士", "HP", "2", "2", "3")]
    [TestCase("魔術師", "HP", "100", "7", "8")]
    [TestCase("僧侶", "MP", "-2", "0", "1")]
    public void testCure(string name, string key, string value, string result, string remain)
    {
        var dl = new DomainLogic();
        dl.ParseResourceData("members", _members);
        dl.Inn(name);
        dl.SetGameData($"members.{name}", $"{key}now", "1");
        var result2 = dl.parse($"{value} {key} {name} :cure");
        Assert.AreEqual(result, result2, "result");
        Assert.AreEqual(remain, dl.GetGameData($"members.{name}", $"{key}now"), "remain");
    }

    [Test]
    public void testFilter()
    {
        var dl = new DomainLogic();
        dl.ParseResourceData("members", _members);
        var result = dl.parse("members.+ vit.filtervalue.system.$.$.10.> :filter");
        Assert.AreEqual("0", result);
    }

    [Test]
    public void testArraySelection()
    {
        var dl = new DomainLogic();
        dl.parse("ゴブリンA ゴブリンA.target.action.:=.attack :sel ");
        Assert.AreEqual("ゴブリンA", dl.GetGameData("select.0", "label"));
        Assert.AreEqual("ゴブリンA.target.action.:=.attack", dl.GetGameData("select.0", "value"));
        var tokens = dl.GetGameData("select.0", "value").Replace('.', ' ');
        var selvalue = dl.parse(tokens);
        Assert.AreEqual("attack", selvalue, "シーン選択1");
        Assert.AreEqual("ゴブリンA", dl.GetGameData("action", "target"), "選択肢1");

        var selvalue2 = dl.parse("guard");
        Assert.AreEqual("guard", selvalue2, "シーン選択2");
    }

    [Test]
    public void testEqual()
    {
        var dl = new DomainLogic();
        var result = dl.parse("ゴブリン 戦士 :eq");
        Assert.AreEqual("0", result);
    }

    [Test]
    public void testListSelect()
    {
        var dl = new DomainLogic();
        dl.parse("戦士.僧侶.魔法剣士.魔術師.盗賊 buttleMember :newlist");
        var okList = new System.Collections.Generic.List<string> { "戦士", "僧侶", "魔法剣士" };
        for(var i = 0; i < 100; i++)
        {
            var result = dl.parse("3 :random buttleMember $");
            Assert.IsTrue(okList.Contains(result), $"result={result}");
        }
    }

    [Test]
    public void testBadStatus()
    {
        var dl = new DomainLogic();
        dl.parse("戦士 target action :=");
        Assert.AreEqual("戦士", dl.GetGameData("action", "target"));
        Assert.AreEqual("0", dl.GetGameData("members.戦士.status", "dead"));
        dl.parse("1 dead status target action $ members . . :=");
        Assert.AreEqual("1", dl.GetGameData("members.戦士.status", "dead"));
    }

    [Test]
    public void testLiveMembers()
    {
        var dl = new DomainLogic();
        dl.parse("1 ゴブリン1 ゴブリン :encounter");
        dl.parse("1 ゴブリン2 ゴブリン :encounter");
        dl.parse("group.1 :tolist :livemembers msg view :=");
        Assert.AreEqual("ゴブリン1.ゴブリン2", dl.GetGameData("view", "msg"));
        dl.parse("1 dead members.ゴブリン1.status :=");
        dl.parse("group.1 :tolist :livemembers msg view :=");
        Assert.AreEqual("ゴブリン2", dl.GetGameData("view", "msg"));
        dl.parse("1 dead members.ゴブリン2.status :=");
        dl.parse("group.1 :tolist :livemembers msg view :=");
        Assert.AreEqual("0", dl.GetGameData("view", "msg"));
    }

    [Test]
    public void testMagicLogic()
    {
        var dl = new DomainLogic();
        dl.ParseResourceData("members", _members);
        dl.SetGameData("action", "actor", "僧侶");
        dl.Inn("僧侶");
        Assert.AreEqual("0", dl.parse("MPnow actor action $ members . $ 3 >"));
        dl.SetGameData("members.僧侶", "MPnow", "3");
        Assert.AreEqual("0", dl.parse("MPnow actor action $ members . $ 3 >"));
        dl.SetGameData("members.僧侶", "MPnow", "2");
        Assert.AreEqual("1", dl.parse("MPnow actor action $ members . $ 3 >"));
    }

    [Test]
    public void testFind()
    {
        var dl = new DomainLogic();
        dl.parse("red.green.blue colors :newlist");
        var result = dl.parse("green colors :find");
        Assert.AreEqual("1", result);
    }

    [Test]
    public void testSetupGroups()
    {
        var dl = new DomainLogic();
        dl.parse("戦士.剣術士.武術士 group.-1 :newlist");
        dl.parse("僧侶.盗賊.魔術師 group.-2 :newlist");
        dl.SetGameData("members.戦士.status", "dead", "1");
        dl.SetGameData("members.剣術士.status", "dead", "1");
        dl.parse(":setupgroups");
        Assert.AreEqual("3", dl.GetGameData("group.-1", "count"));
        Assert.AreEqual("3", dl.GetGameData("group.-2", "count"));
        dl.SetGameData("members.武術士.status", "dead", "1");
        dl.parse(":setupgroups");
        Assert.AreEqual("3", dl.GetGameData("group.-1", "count"));
        Assert.AreEqual("0", dl.GetGameData("group.-2", "count"));
        Assert.AreEqual("僧侶", dl.GetGameData("group.-1", "0"));
    }
}
